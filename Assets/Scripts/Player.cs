﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class Player : PhysicsCollision
{
    [Header("Properties")]
    public float speed;
    float horizontalAxis;
    float normalizedMovement;
    bool jump;
    public float jumpForce;
   
 
    protected override void Start()
    {
        base.Start();
    }
    void Update()
    {
        HorizontalMovement();
    }
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
 
        if(jump)
        {
            rb.velocity = new Vector3(rb.velocity.x, 0, 0);
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            jump = false;
        }
 
        rb.velocity = new Vector3(normalizedMovement, rb.velocity.y, 0);
    }

    /*protected override void CheckGround(){
        Teleport();
    }  */
 
 
    public void SetAxis(float h)
    {
        horizontalAxis = h;
    }
    public void JumpStart()
    {
        if(isGrounded) jump = true;
    }

    public void Teleport(){
        transform.position = new Vector3 (0, 5, 0);
        Debug.Log("cola");
    }
 
    void HorizontalMovement()
    {
        if(horizontalAxis > 0 && !isFacingRight) Flip();
        else if(horizontalAxis < 0 && isFacingRight) Flip();
 
        if(isTouchingWall)
        {
            normalizedMovement = 0;
            return;
        }
 
        normalizedMovement = horizontalAxis * speed;
    }

    public void Run(){
        normalizedMovement = horizontalAxis * speed * 2;
    }
 
}